module no.uib.ii.inf112 {
	requires javafx.graphics;
	requires turtleduck.base;
	requires turtleduck.gl;
	requires java.xml;
	requires java.management;
	exports pond.model;
	exports pond.view;
	exports pond.controller;
}