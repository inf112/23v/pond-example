package pond;
import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		List<IDuck> duckies = new ArrayList<IDuck>();
		List<Duck> ducks = new ArrayList<Duck>();
		int i = 0;

		IDuck d1 = duckies.get(i);

		IDuck d2 = ducks.get(i);

		// Duck d3 = duckies.get(i);

		// Frog f1 = duckies.get(i);

		// ducks.add(new Frog());

		duckies.add(new Frog());

		// ArrayList<IDuck> list1 = duckies;

		ArrayList<IDuck> list2 = (ArrayList<IDuck>) duckies;

		Duck d4 = (Duck) duckies.get(i);

		if (duckies.get(i) instanceof Duck) {
			Duck d5 = (Duck) duckies.get(i);
			/* gjør andeting */
		} else if (duckies.get(i) instanceof Frog) {
			Frog f2 = (Frog) duckies.get(i);
			/* gjør frosketing */
		}

		// List<IDuck> list3 = ducks;
		// List<Duck> list4 = duckies;
	}
}

interface IDuck {
}

class Duck implements IDuck {
}

class Frog implements IDuck {
}