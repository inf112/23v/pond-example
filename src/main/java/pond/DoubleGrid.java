package pond;

import org.joml.Vector2d;
import org.joml.Vector2dc;

public class DoubleGrid {
//		public static final double C1 = 6.0/18.0, C2 = 2.0/18.0, C3 = 1.0/18.0;
	public static final double C1 = 4.0 / 9.0, C2 = 1.0 / 9.0, C3 = 1.0 / 36;

	static {
		double cTotal = 0;
		for(Dir dir : Dir.values())
			cTotal += dir.coeff;
		System.out.println("C1+4*C2+4*C3=" + cTotal);
	}
	public enum Dir {
		C(0,0,0,C1), E(1,1,0,C2), N(2,0,-1,C2), W(3, -1, 0,C2), S(4,0,1,C2),
		NE(5,1,-1,C3), NW(6,-1,-1,C3), SW(7,-1,1,C3), SE(8,1,1,C3);
		static Dir REFLECT[] = {C,W,S,E,N,SW,SE,NE,NW};
		static Dir REFLECT_X[] = {C,W,N,E,S,NW,NE,SE,SW};
		static Dir REFLECT_Y[] = {C,E,S,W,N,SE,SW,NW,NE};
		int n;
		int dx, dy;
		private double coeff;
		private Vector2dc vec;
		private Dir(int n, int dx, int dy, double coeff) {
			this.n = n;
			this.dx = dx;
			this.dy = dy;
			this.coeff = coeff;
			this.vec = new Vector2d(dx, dy);
		}
		public Dir reflect() {
			return REFLECT[n];
		}
		public Dir reflectX() {
			return REFLECT_X[n];
		}
		public Dir reflectY() {
			return REFLECT_Y[n];
		}
		public double coeff() {
			return coeff;
		}
		public Vector2dc vec() {
			return vec;
		}
	}
	public static int DIRECTIONS = 10;
	// C, NW, N, NE, E, W, SE, S, SW
//	public static final int DX[] = { 0, -1, 0, 1, 1, -1, 1, 0, -1 };
//	public static final int DY[] = { 0, -1, -1, -1, 0, 0, 1, 1, 1 };
//	public static final int REFLECT[] = { 0, 8, 7, 6, 5, 4, 3, 2, 1 };
//	public static final int REFLECT_X[] = { 0, 3, 2, 1, 5, 4, 8, 7, 6 };
//	public static final int REFLECT_Y[] = { 0, 6, 7, 8, 4, 5, 1, 2, 3 };
//	public static final Vector2d E[] = new Vector2d[9];
	double[] data;
	int width, height;


	public DoubleGrid(int width, int height) {
		this.width = width;
		this.height = height;
		this.data = new double[width * height * DIRECTIONS];
	}

	public double get(int x, int y) {
		return get(x, y, Dir.C);
	}

	public double getNeigh(int x, int y, Dir neigh) {
		return getNeigh(x, y, neigh, Dir.C);
	}

	public double getNeigh(int x, int y, Dir neigh, Dir dir) {
		return get(x+dir.dx, y+dir.dy, dir);
	}

	public void set(int x, int y, double v) {
		set(x, y, Dir.C, v);
	}

	public double get(int x, int y, Dir dir) {
//		if ((x < 0 || x >= width) && (y < 0 || y >= height))
//			dir = dir.reflect();
//		else if (x < 0 || x >= width)
//			dir = dir.reflectX();
//		else if (y < 0 || y >= height)
//			dir = dir.reflectY();
//		if(x < 0 || x >= width || y < 0 || y >= height)
//			return 0;
//		else
		return data[index(x, y) + dir.n];
	}

	public int index(int x, int y) {
//		if (true) {
//			x = Math.max(0, Math.min(width - 1, x));
//			y = Math.max(0, Math.min(height - 1, y));
//		} else {
//			if (x < 0)
//				x += width;
//			if (x >= width)
//				x -= width;
//			if (y < 0)
//				y += height;
//			if (y >= height)
//				y -= height;
//		}
		return (y * width + x) * DIRECTIONS;
	}

	public void set(int x, int y, Dir dir, double v) {
		if (x >= 0 && x < width && y >= 0 && y < height)
			data[index(x, y) + dir.n] = v;
	}

	public void setNeigh(int x, int y, Dir neigh, Dir dir, double v) {
		set(x+neigh.dx, y+neigh.dy, dir, v);
//		data[((DY[neigh] + y) * width + (DX[neigh] + x)) * DIRECTIONS + dir] = v;
	}

	public int width() {
		return width;
	}

	public int height() {
		return height();
	}

	public void setAll(double v) {
		for (int i = 0; i < width * height * DIRECTIONS; i += DIRECTIONS)
			data[i] = v;
	}

	public void flip() {
//		double[] tmp = data;
//		data = data2;
//		data2 = tmp;
		System.arraycopy(data, 0, data, 0, data.length);
	}
}
