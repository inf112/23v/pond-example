package pond.controller;

import turtleduck.geometry.Point;

public interface ModelUpdateListener {
	void positionUpdated(Point newPos);
}
