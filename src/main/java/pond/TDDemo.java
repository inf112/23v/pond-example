package pond;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import pond.model.Pond;
import pond.model.PondObject;
import pond.view.Curves;
import pond.view.PondView;
import pond.view.Viewable;
import turtleduck.FrameInfo;
import turtleduck.TurtleDuckApp;
import turtleduck.canvas.Canvas;
import turtleduck.colors.Colors;
import static turtleduck.colors.Colors.*;
import static turtleduck.geometry.Point.point;

import turtleduck.colors.Color;
import turtleduck.display.Screen;
import turtleduck.events.Controls;
import turtleduck.events.InputControl;
import turtleduck.events.KeyCodes;
import turtleduck.geometry.Direction;
import turtleduck.geometry.Point;
import turtleduck.gl.GLLauncher;
import turtleduck.gl.GLLayer;
import turtleduck.gl.GLPixelData;
import turtleduck.gl.GLScreen;
import turtleduck.image.Image;
import turtleduck.image.ImageFactory;
import turtleduck.image.TileAtlas;
import turtleduck.paths.Pen;
import turtleduck.sim.StepwiseRunner;
import turtleduck.turtle.Turtle;

/**
 * A demo application
 * 
 * @author anya
 *
 */
public class TDDemo implements TurtleDuckApp {
	/**
	 * The currently running demo
	 */
	private static TDDemo demo;
	/**
	 * How many frames we normally want to render per second
	 */
	private static final int DEFAULT_FPS = 20;
	/**
	 * Number of nanoseconds in a second
	 */
	private static final long NANOS_PER_SEC = 1_000_000_000;
	/**
	 * How many frames we want to render per second
	 */
	private int targetFps = DEFAULT_FPS;
	/**
	 * How many nanoseconds to spend per frame, in order to reach {@link #targetFps}
	 */
	private long nanosPerFrame = NANOS_PER_SEC / targetFps;
	/**
	 * Running average of time spent spent per frame
	 */
	private long runningAverageTimePerFrame = nanosPerFrame;
	/**
	 * Time stamp of last frame drawn (or 0 for the first frame)
	 */
	private long lastUpdateTime = 0L;
	/**
	 * How long we slept last time
	 */
	private long lastSleep = 0L;
	/**
	 * Sequential numbering of frames
	 */
	private long frameCounter = 0L;
	/**
	 * The Screen we're drawing to
	 */
	private Screen screen;
	/**
	 * True if we're currently paused
	 */
	boolean paused;
	private List<InputControl<Float>> axes = new ArrayList<>();

	/**
	 * A duck pond
	 */
	public Pond pondModel;
	private PondView pondView;
	private Canvas canvas;
	private Pen pen;
	private Turtle turtle3;
	private Turtle turtle;
	private int step = 0;
	private Image image;
	private int fieldCount = 0;
	private int frameCount = 0;

	/**
	 * @return The currently running demo
	 */
	public static TDDemo getInstance() {
		return demo;
	}

	/**
	 * Start the application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// new JfxLauncher().app(new TDDemo()).launch(args);
		System.out.println("TDDemo main!");
		new GLLauncher().app(new TDDemo()).launch(args);
	}

	private void unpause() {
		paused = false;
	}

	private void pause() {
		paused = true;
	}

	List<Point> bezierPoints = new ArrayList<>();
	int bpPos = 0;
	private TileAtlas atlas;
	private double time;
	private LatticeWater water;
	private StepwiseRunner<DoubleGrid> waterRunner;

	public void redraw() {
//		canvas.color(Colors.GRAY.opacity(0.3)).rectangle().at(Point.point(0, 0)).width(1280).height(720).stroke();
		canvas.clear();
		for (int i = 0; i < 360; i++) {
			Point point2 = Point.ZERO.add(Direction.absolute(i % 360), 300);
			Curves.drawBezier(turtle.spawn().color(Colors.WHITE).jumpTo(0, 0).turnTo(i % 360), 200, 200, i % 360,
					point2);
		}
	}

	protected void drawFrame2() {


//		if (frameCount % 10 == 0) {

//		}
		GLLayer layer = ((GLScreen)screen).getGLLayer(); // canvas instanceof GLLayer ? (GLLayer) canvas : null;
		if (layer != null)
			layer.drawHeightMap(point(0, 0, -10), 1280, 720, 200, water.width, water.height, water.density());
		pondModel.draw(canvas, turtle);

		if (false) {
			stem(turtle.spawn().jumpTo(0, 320).turnTo(90), 4);
			turtle.color(Colors.WHITE).strokeOnly();
			turtle.jumpTo(400, 150).turnTo(-90 * frameCount);
			drawSnowFlake(turtle, 50, 0, 3);
			turtle.jumpTo(200, -150).turnTo(-90 * frameCount);
			drawSnowFlake(turtle, 50, 1, 3);
			turtle.jumpTo(0, 150).turnTo(-90 * frameCount);
			drawSnowFlake(turtle, 50, 2, 4);
			turtle.jumpTo(-200, -150).turnTo(-90 * frameCount);
			drawSnowFlake(turtle, 50, 3, 3);
		}

	}

	private void drawAxes() {
		int w = (int) screen.width() / 20 * 10;
		int h = (int) screen.height() / 20 * 10;
		canvas.stroke(Colors.RED, 1);
		int l = 5;
		for (int x = -w; x <= w; x += 10) {
			if (x % 100 == 0)
				canvas.drawLine(Point.point(x, l), Point.point(x, -l));
			else
				canvas.drawPoint(Point.point(x, 0));
		}
		canvas.drawLine(Point.point(w - 10, -l), Point.point(w, 0));
		canvas.drawLine(Point.point(w - 10, l), Point.point(w, 0));
		for (int y = -h; y <= h; y += 10) {
			if (y % 100 == 0)
				canvas.drawLine(Point.point(-l, y), Point.point(l, y));
			else
				canvas.drawPoint(Point.point(0, y));
		}
		canvas.drawLine(Point.point(-l, h - 10), Point.point(0, h));
		canvas.drawLine(Point.point(l, h - 10), Point.point(0, h));
	}

	void petal(Turtle turtle, int a, double s) {
		turtle.turn(-a);
		for (int i = a; i < 360 - a; i++) {
			turtle.draw(s).turn(1);
			if (i == 180)
				turtle.turn(a);
		}
	}

	void flower(Turtle turtle) {
		turtle.fill(Colors.YELLOW.perturb()).stroke(Colors.TRANSPARENT);
		for (int i = 0; i < 5; i++) {
			petal(turtle.turn(72).spawn().jump(1), 65, .25);
			turtle.spawn().stroke(Colors.MAROON, 1).draw(5);
		}
	}

	void stem(Turtle turtle, int n) {
		turtle.stroke(Colors.GREEN).fill(Colors.TRANSPARENT);
		if (n <= 0) {
			return;
		}
		turtle.strokeWidth(n);
		turtle.draw(45 * n);
		if (n > 1) {
			stem(turtle.spawn().turn(-45), n - 2);
			stem(turtle.spawn().turn(1/* rand(-5, 5) */), n - 1);
			stem(turtle.spawn().turn(45), n - 1);
		} else {
			flower(turtle.spawn());
		}

//		canvas.rectangle().at(10, 50).width(50).height(100).color(PINK).fill(c -> c.darker()).strokeAndFill();
	}

	public Screen getScreen() {
		return screen;
	}

	@Override
	public void update(FrameInfo info) {
		time += info.deltaTime();

		// Update the model
		if (time > 0.04) {
			step = (step + 1) % 359 + 1;
			pondModel.step();
			time -= .04;
//			if (time > 0.04)
//				System.out.println("time left over: " + time);
		} else {
//			System.out.println("time short: " + time);

		}
		// Update the view
		// 	public void draw(List<Viewable> viewables) {
		// 	public List<PondObject> objects() {
		// public interface PondObject extends Viewable {
		// FEIL: draw(List<Viewable>) not applicable for List<PondObject>
		pondView.draw(pondModel.objects());
		
		fieldCount++;
		frameCount = fieldCount / 4;
	}


	@Override
	public void start(Screen screen) {
		demo = this;
		this.screen = screen;
		try {
			loadTiles();
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int key : Arrays.asList(KeyCodes.GamePad.AXIS_RIGHT_X, KeyCodes.GamePad.AXIS_RIGHT_Y,
				KeyCodes.GamePad.AXIS_LEFT_X, KeyCodes.GamePad.AXIS_LEFT_Y)) {
			InputControl<Float> control = this.screen.inputControl(Float.class, key, 1);
			if (control == null) {
				control = Controls.create("", () -> 0f);
			}
			axes.add(control);
		}

//		screen.setFullScreen(true);
		System.out.println("starting");
		image = ImageFactory.defaultFactory().imageFromResource("/turtleduck-icon.png", 256, 256, null);
		System.out.println(image);
		canvas = screen.createCanvas();
		pen = canvas.pen();
		turtle3 = canvas.turtle3();
		turtle = canvas.turtle();
		pondModel = new Pond();
		pondModel.setup((int) screen.width(), (int) screen.height());
		pondView = new PondView(canvas, turtle);
		pondView.setup();
		water = new LatticeWater(500, 500);
		waterRunner = StepwiseRunner.create();
		waterRunner.initialize(water);
		waterRunner.stepsPerSec(40);
		waterRunner.start();
		System.out.println("width: " + screen.width() + ", height: " + screen.height());
	}

	public void loadTiles() throws SAXException, IOException, ParserConfigurationException {
		Image sheet = new GLPixelData("/tiles_spritesheet.png");
		atlas = TileAtlas.create(sheet);
		try (InputStream stream = getClass().getResourceAsStream("/tiles_spritesheet.xml")) {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(stream);

			NodeList nodeList = document.getElementsByTagName("TextureAtlas");
			for (int i = 0; i < nodeList.getLength(); i++) {
				NodeList children = nodeList.item(i).getChildNodes();
				for (int j = 0; j < children.getLength(); j++) {
					Node n = children.item(j);
					if (n.getNodeType() == Node.ELEMENT_NODE) {
						Element texElt = (Element) n;
						if (texElt.getTagName().equals("SubTexture")) {
							String name = texElt.getAttribute("name");
							String xstr = texElt.getAttribute("x");
							String ystr = texElt.getAttribute("y");
							String widthstr = texElt.getAttribute("width");
							String heightstr = texElt.getAttribute("height");
							int x = Integer.valueOf(xstr);
							int y = Integer.valueOf(ystr);
							int width = Integer.valueOf(widthstr);
							int height = Integer.valueOf(heightstr);
							System.out.printf("%s: at %d,%d size %dx%d%n", name, x, y, width, height);
							atlas.add(name, x, y, width, height);
						}
					}
				}
			}
		}

	}

	public void drawSnowFlake(Turtle turtle, double size, int variant, int depth) {
		if (depth > 0) {
//			turtle.strokeWidth(depth);
			variant = Math.abs((variant) % 4);
//			turtle.color(c -> c.mix(Colors.TRANSPARENT, .2));
			// painter.turn(60);
			switch (variant) {
			case 0:
				for (int i = 0; i < 6; i++) {
					turtle.turn(60);
					drawSnowFlake(turtle.spawn().draw(size / 2), size / 2.5, 2, depth);
				}
				break;
			case 1:
				for (int i = 0; i < 6; i++) {
					turtle.turn(60);
					drawSnowFlake(turtle.spawn().draw(size).turn(-90), size / 1.5, variant, depth - 1);
				}
				break;
			case 2:
				turtle.turn(-90);
				for (int i = 0; i < 3; i++) {
					turtle.turn(45);
					drawSnowFlake(turtle.spawn().draw(size), size / 1.25, 3, depth - 1);
				}
				break;
			case 3:
			default:
				turtle.turn(-90);
				for (int i = 0; i < 6; i++) {
					turtle.turn(60);
					drawSnowFlake(turtle.spawn().draw(size), size / 1.5, variant, depth - 1);

				}
				break;

			}
		}
	}
}
