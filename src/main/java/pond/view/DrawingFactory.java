package pond.view;

public interface DrawingFactory {
	String name();
	Drawing create(String argument);
}
