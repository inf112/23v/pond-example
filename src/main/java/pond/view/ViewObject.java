package pond.view;

import turtleduck.geometry.Point;

public interface ViewObject {
	Point position();
	String viewRef();
}
