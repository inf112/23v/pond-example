package pond.view;

import turtleduck.canvas.Canvas;

public class ImageDrawing implements Drawing {

	private String imgName;
	public ImageDrawing(String imgName) {
		this.imgName = imgName;
	}
	@Override
	public void draw(Viewable v, Canvas canvas) {
		canvas.image().at(v.position()).width(v.width()).height(v.height()).done();
	}

}
