package pond.view;

import turtleduck.canvas.Canvas;
import turtleduck.colors.Colors;
import turtleduck.colors.Color;

public class EllipseDrawing implements Drawing {

	static {
		
	}
	private Color color;
	
	public static DrawingFactory getFactory() {
		
		//return (arg) -> new EllipseDrawing(arg);
		return new DrawingFactory() {

			@Override
			public String name() {
				return "ellipse";
			}

			@Override
			public Drawing create(String argument) {
				return new EllipseDrawing(argument);
			}
		};
	}
	
	public EllipseDrawing(String color) {
		this.color = Color.fromString(color);
	}
	
	
	@Override
	public void draw(Viewable v, Canvas canvas) {
		canvas.ellipse().at(v.position()) //
		.width(v.width())//
		.height(v.height()) //
		.color(color) //
		.fill();
	}

}
