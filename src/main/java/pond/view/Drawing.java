package pond.view;

import turtleduck.canvas.Canvas;

public interface Drawing {
	void draw(Viewable v, Canvas canvas);
	
}
