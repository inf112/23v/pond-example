package pond.view;

import turtleduck.geometry.Point;

public interface Viewable {
	Point position();
	double size();
	double width();
	double height();
	String viewRef();
}
