package pond.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pond.model.PondObject;
import pond.model.Frog;
import pond.model.Duck;
import turtleduck.canvas.Canvas;
import turtleduck.colors.Colors;
import turtleduck.geometry.Point;
import turtleduck.turtle.Turtle;

public class PondView {
	private Canvas canvas;
	private Turtle turtle;
	private Map<String, DrawingFactory> dFactories = new HashMap<>();

	public PondView(Canvas canvas, Turtle turtle) {
		this.canvas = canvas;
		this.turtle = turtle;
	}

	public void setup() {
		DrawingFactory factory = EllipseDrawing.getFactory();
		dFactories.put(factory.name(), factory);
		//drawings.put("frog", new EllipseDrawing(Colors.GREEN));
		//drawings.put("duck", new EllipseDrawing(Colors.BROWN));
	}

	// 'viewables' – liste med ting som skal vises
	// * kunne vært PondObject (fells interface for Duck og Frog)
	// * AbstractPondDweller (abstract superklasse som begge arver fra)
	//
	// MEN: viktig designprinsipp er å ikke være avhengig av "unødvendige" ting
	// Viewble inneholder akkurat det vi trenger i viewet, PondObject inneholder
	// også ting som er relevant for modellen.
	public void draw(Iterable<? extends Viewable> viewables) {
		/*
		 * int x = switch(42) { case 33 > 2; }
		 */

		for (Viewable item : viewables) {
			String[] split = item.viewRef().split(":", 2);
			DrawingFactory drawingFactory = dFactories.get(split[0]);
			Drawing drawing = drawingFactory.create(split[1]);
			// Drawing drawing = drawings.get(item.viewRef());
			if (drawing != null)
				drawing.draw(item, canvas);
		}
	}
}
