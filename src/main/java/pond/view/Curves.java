package pond.view;

import turtleduck.colors.Colors;
import turtleduck.geometry.Direction;
import turtleduck.geometry.Point;
import turtleduck.turtle.Turtle;

public class Curves {
	public static Turtle arc(Turtle turtle, double angle, double arcLength) {
		double r = arcLength / Math.toRadians(angle);
		double crd = 2 * Math.sin(Math.toRadians(angle) / 2);
		turtle.spawn().turn(angle / 2).strokeWidth(3).color(Colors.RED).draw(r * crd);
//		turtle.turn(-angle / 2);
		int steps = 10;
		for (int i = 0; i < steps; i += 1) {
			turtle.turn(.5 * angle / steps).draw(arcLength / steps).turn(.5 * angle / steps);
		}
//		turtle.turn(-angle / 2);
		return turtle;
	}

	public static Turtle arc2(Turtle turtle, double angle, double chordLength) {

		double crd = 2 * Math.sin(Math.toRadians(angle) / 2);
		double r = chordLength / crd;
		double arcLength = Math.toRadians(angle) * r;
		turtle.spawn().turn(angle / 2).strokeWidth(3).color(Colors.RED).draw(chordLength);
//		turtle.turn(-angle / 2);
		int steps = 10;
		for (int i = 0; i < steps; i += 1) {
			turtle.turn(.5 * angle / steps).draw(arcLength / steps).turn(.5 * angle / steps);
		}
//		turtle.turn(-angle / 2);
		return turtle;
	}

	public static Turtle arcTo(Turtle turtle, double angle, Point to) {
		double chordLength = turtle.point().distanceTo(to);
		if (chordLength < 0.0001)
			return turtle;
		Direction dirT = turtle.direction().yaw(180);
		Direction dirP = turtle.point().directionTo(to);
		double a = dirT.sub(dirP).degrees();
		angle = 2 * a;
//		return arc2(turtle.turnTo(turtle.point().directionTo(to)), angle, dist).turn(angle/2);
		double crd = 2 * Math.sin(Math.toRadians(angle) / 2);
		double r = chordLength / crd;
		double arcLength = Math.toRadians(angle) * r;
		turtle.spawn().strokeWidth(3).color(Colors.RED).draw(chordLength);
		turtle.spawn().strokeWidth(3).color(Colors.GREEN).drawTo(to);
		int steps = 10;
		for (int i = 0; i < steps; i += 1) {
			turtle.turn(.5 * angle / steps).draw(arcLength / steps).turn(.5 * angle / steps);
		}
		return turtle;
	}

	public static Turtle drawBezier(Turtle turtle, double ctrl1Dist, double ctrl2Dist, double ctrl2Angle, Point dest) {
		Point ctrl2 = dest.add(Direction.absolute(-ctrl2Angle), ctrl2Dist);
		return drawBezier(turtle, ctrl1Dist, ctrl2, dest);

	}

	public static Turtle drawBezier(Turtle turtle, double ctrl1Dist, Point ctrl2, Point dest) {
		Point ctrl1 = turtle.spawn().jump(ctrl1Dist).point();
		Point src = turtle.point();
		double n = 20;
		for (int i = 0; i <= n; i++) {
			double f = i / n;
			Point p1 = src.interpolate(ctrl1, f);
			Point p2 = ctrl1.interpolate(ctrl2, f);
			Point p3 = ctrl2.interpolate(dest, f);
			Point p4 = p1.interpolate(p2, f);
			Point p5 = p2.interpolate(p3, f);
			Point p6 = p4.interpolate(p5, f);
			turtle.drawTo(p6);
		}
		turtle.endPath();
		return turtle;
	}

}
