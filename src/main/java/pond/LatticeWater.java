package pond;

import java.util.Arrays;
import java.util.function.Consumer;

import org.joml.Vector2d;

import pond.model.Frog;
import pond.model.Pond;
import pond.model.PondObject;
import turtleduck.geometry.Point;
import turtleduck.sim.StepwiseSimulation;

public class LatticeWater implements StepwiseSimulation<DoubleGrid> {
	Vector2d[] velocity;
	private double[] density, density2;
	double totalDensity = 0, originalDensity = 0;
	private Vector2d tmp = new Vector2d();
	int width;
	int height;
	double densityAdjust = 1;
	Consumer<Vector2d[]> modifyVelocity;
	int dropX, dropY, drop = 0;

	public LatticeWater(int width, int height) {
		this.width = width;
		this.height = height;
		velocity = new Vector2d[width * height];
		density = new double[width * height];
		density2 = new double[width * height];
	}

	@Override
	public void initialize(DoubleGrid state1, DoubleGrid state2) {
		if (true)
			return;
		for (int i = 0; i < velocity.length; i++)
			velocity[i] = new Vector2d();

		for (int y = 0; y < height - 1; y++) {
			for (int x = 0; x < width - 1; x++) {
				double z = Math.sin(2 * Math.PI * y / 50.0) + Math.cos(2 * Math.PI * x / 50.0);
				double state = (1 + Math.abs(z) / 3) / 10;
				state = 0;
				// state2.set(x, y, state);
//				state1.set(x, y, state);
				density2[x + width * y] = 0;// 0.25 + state;
			}
		}
		state2.set(50, 50, .1);
		state2.set(50, 51, 0);

	}

	@Override
	public void step(int stepNum, DoubleGrid current, DoubleGrid last) {
		double total = 0;
		for (int y = 1; y < height - 1; y++) {
			for (int x = 1; x < width - 1; x++) {
//				current.set(x, y, last.get(x, y));
				double neighbours = (last.get(x - 1, y)//
						+ last.get(x + 1, y)//
						+ last.get(x, y - 1)//
						+ last.get(x, y + 1));
//				if(state != 0)
//					System.out.println(state + ", " + current.get(x, y));
				double state = neighbours / 2 - current.get(x, y);
				// state -= state / 960;
				if (x == 1 || x == width - 2 || y == 1 || y == height - 2)
					state = 0;
				state = (100 * state + neighbours) / 104; // dampen
				// if(x > width-10)
				// state = state *1.1;
				for (PondObject obj : TDDemo.getInstance().pondModel.objects()) {
					if (obj instanceof Frog f) {
						double fx = width * (obj.getX() + 640) / 1280.0;
						double fy = height * (-obj.getY() + 320 + obj.height() * 1.3) / 720.0;
						double d1 = Math.pow(fx - x -6, 2) + Math.pow(fy - y, 2);
						double d2 = Math.pow(fx - x +6, 2) + Math.pow(fy - y, 2);
						// System.out.println(obj.position() + ", " + x + ", " + y);
						if (d1 < 30 || d2 < 30) { // x > 0 && x < width && y > 0 && y < height) {
							if (!f.jumping) {
								state = 0.0;
							} else if (f.linearStep <= 0.05) {
								state -= f.linearStep;
							} else if (f.linearStep > 0.90) {
								state -= (f.linearStep-.90)/5.0;
							}
						}

					}
				}
				current.set(x, y, state);
				density2[x + width * y] = state; // (neighbours+state)/5;
				total += state;
			}
		}
		if (drop < 1 && Math.random() < 0.25) {
			dropX = 1 + (int) (Math.random() * (width - 2));
			dropY = 1; // 1 + (int) (Math.random() * (height - 2));
//			double h = -(.025 + Math.random() / 5);
//			System.out.print("\n");
//			current.set(dropX, dropY, h);
			drop = 50;
		}
		if (drop-- > 1) {
			double h = 1.5 * Math.sin(2 * drop * Math.PI / 50) * drop / 50.0;
			// System.out.printf(" %.2f",h);
//			current.set(dropX, dropY, h);
			// dropX += 10;
		}
		// synchronized (this) {

		// }
//		current.set(50, 50, -1);
		// current.set(98, 99, 2 + Math.random() / 5);
		synchronized (this) {
			totalDensity = total;
//			System.out.println("Total density: " + totalDensity);
		}

	}

	@Override
	public DoubleGrid createState(int stepNum) {
		return new DoubleGrid(width, height);
	}

	public synchronized void modifyVelocity(Consumer<Vector2d[]> modifier) {
		this.modifyVelocity = modifier;
	}

	double densityAt(int x, int y) {
		return density2[x + width * y];
	}

	public double[] density() {
		synchronized (density2) {
			for (int y = 1; y < height - 1; y++) {
				for (int x = 1; x < width - 1; x++) {
					double neigh = densityAt(x - 1, y) + densityAt(x + 1, y) + densityAt(x, y - 1)
							+ densityAt(x, y + 1);
					double diag = densityAt(x - 1, y - 1) + densityAt(x - 1, y + 1) + densityAt(x + 1, y - 1)
							+ densityAt(x + 1, y + 1);
					density[x + width * y] = //
							// (neigh/8 + diag + densityAt(x, y))/9;
							(neigh / 8 + diag / 16 + densityAt(x, y) / 4);
				}
			}

			// System.arraycopy(density2, 0, density, 0, density.length);
			return density; // Arrays.copyOf(density, density.length);
		}
	}
}
