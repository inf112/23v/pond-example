package pond.model;

import java.util.ArrayList;
import java.util.List;

import pond.controller.ModelUpdateListener;
import turtleduck.colors.Color;
import turtleduck.colors.Colors;
import turtleduck.geometry.Point;
import turtleduck.paths.Pen;
import turtleduck.turtle.Turtle;

// abstract class – noen metoder kan mangle, men du kan ha implementasjon til
// andre metoder, og du kan ha feltvariabler
// du kan aldri lage objekter av en abstrakt klasse eller et interface
public abstract class AbstractPondDweller implements PondObject {
	protected List<ModelUpdateListener> listeners = new ArrayList<>();
	protected double size = 1;
	protected MyPoint pos = new MyPoint(0, 0);
	private double health = 1.0;
	protected Color bodyColor = getPrimaryColor();
	protected Color headColor = getPrimaryColor().darker();
	protected Pen healthBg = Pen.create().strokeWidth(8).stroke(Colors.BLACK).done();
	protected Pen healthGreen = Pen.create().strokeWidth(5).stroke(Colors.GREEN).done();
	protected Pen healthRed = Pen.create().strokeWidth(4.5).stroke(Colors.RED).done();

	protected abstract Color getPrimaryColor();

	public void step() {
		// pos.move(0, 5);
		health = health * 0.999;
	}

	public double size() {
		return size;
	}
	protected void drawHealth(Turtle turtle) {
		turtle.jumpTo(pos.getX(), pos.getY() + height());
		turtle.pen(healthBg);
		turtle.draw(50);
		turtle.pen(healthGreen);
		turtle.draw(health * 50);
		turtle.pen(healthRed);
		turtle.draw((1 - health) * 50);
	}

	public Point position() {
		return Point.point(pos.getX(), pos.getY());
	}
	
	protected void positionUpdated() {
		for(var l : listeners) {
			l.positionUpdated(position());
		}
	}
	public void addListener(ModelUpdateListener listener) {
		listeners.add(listener);
	}
}
