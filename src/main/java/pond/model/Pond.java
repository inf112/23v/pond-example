package pond.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import turtleduck.canvas.Canvas;
import turtleduck.turtle.Turtle;

public class Pond {
	private double x, y;
	private double width, height;

	public List<PondObject> objs = new ArrayList<>();

	public static Random random = new Random();

	public double width() {
		return width;
	}
	public double height() {
		return height;
	}
	public void step() {
		/*
		 * for (Object o : objs) { // vi må si at det er en and (eller frosk) for å få
		 * lov av kompilatoren til å // kalle // metoden – selv om vi vet at alle
		 * objektene våre har step-metoden if (o instanceof IPondObject) ((IPondObject)
		 * o).step(); }
		 * 
		 */
		for (PondObject o : new ArrayList<>(objs)) {
			 o.step(this);
		}
	}
	public void add(PondObject obj) {
		objs.add(obj);
	}

	public List<PondObject> objects() {
		return objs;
	}

	public void setup(int w, int h) {
		width = w;
		height = h;
//		objs.add(new Duck(200, 200));
//		objs.add(new Duck(200, 300));
//		objs.add(new Duck(1200, 600));
//		objs.add(new Duck(0, 0));
//		objs.add(new Duck(-120, -130));
		int n = 10;// 0+random.nextInt(100);
		for (int i = 0; i < n/2; i++) {
			objs.add(new Duck(random.nextInt(w)-w/2, random.nextInt(h)-h/2));
		}
		for (int i = 0; i < n; i++) {
//			objs.add(new T());

			objs.add(new Frog(random.nextInt(w)-w/2, random.nextInt(h)-h/2));
		}
//		objs.add(new Duck(screen.getWidth() / 2, screen.getHeight() / 2));
//		objs.add(new Duck(400, 200));
	}
	
	public void draw(Canvas canvas, Turtle turtle) {
		// TODO Auto-generated method stub
		for (PondObject o : objects()) {
			o.draw(canvas, turtle.spawn());
		}
	}
}
