package pond.model;

import org.joml.Vector3dc;

import pond.controller.ModelUpdateListener;
import turtleduck.colors.Color;
import turtleduck.colors.Colors;
import turtleduck.geometry.Direction;
import turtleduck.geometry.Point;

public class Duckling extends Duck implements ModelUpdateListener{
	public Duckling(MyPoint pos, double size) {
		super(pos, size);
	}

	public void imprint(PondObject obj) {
		obj.addListener(this);
	}
	protected Color getPrimaryColor() {
		return Colors.YELLOW;
	}
	
	public void step() {
		pos.move(2, 0);
	}

	@Override
	public void positionUpdated(Point newPos) {
		Direction dir = position().directionTo(newPos);
		Vector3dc vector = dir.directionVector();
		deltaX = vector.x();
		deltaY = vector.y();
	}
}
