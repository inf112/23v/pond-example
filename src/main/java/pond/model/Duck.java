package pond.model;


import turtleduck.canvas.Canvas;
import turtleduck.colors.Color;
import turtleduck.colors.Colors;
import turtleduck.geometry.Direction;
import turtleduck.geometry.Point;
import turtleduck.turtle.Turtle;

/** A duck class – recipe for making duck objects.
 * 
 * Describes the common aspects of and behaviour of duck objects (in methods)
 * and the individual differences between ducks (in field variables). */
public class Duck extends AbstractPondDweller  {
	// Dette er datastrukturen (sett av feltvariabler) som lagrer tilstanden til andeobjektene:
	// "private" betyr at vi ønsker at dette skal være skjult, og at feltvariablene ikke kan
	// endres eller leses av objekter (av andre klasser)
	private boolean swimming = true;
	private int stepCount = 0;
	protected double deltaX, deltaY;
	public Duck() {
		this(500, 200);
		this.deltaX = Math.random()-.5;
		this.deltaY = Math.random()-.5;
	}
	
	public void step(Pond world) {
		if(stepCount++ > 50) {
			stepCount = 0;
//			world.objs.add(new Duckling(new MyPoint(pos.getX(), pos.getY()), (Pond.random.nextDouble()+1)*size/3.0));
		}
		pos.move(deltaX, deltaY);
		positionUpdated();
	}
	
	protected Color getPrimaryColor() {
		return Colors.BROWN;
	}
	public Duck(double x, double y) {
		this.pos = new MyPoint(x, y);
		this.swimming = x > 600;
		if(x > 600)
			this.swimming = true;
		else
			this.swimming = false;
	}
	public Duck(MyPoint pos, double size) {
		this.pos = pos;
		this.size = size;
	}
	
	public Duck(double x, double y, double size) {
		this(x,y);
		this.size = size;
	}
	
	
	
	/**
	 * @return The duck's width 
	 */
	public double width() {
		return 100 * size;
	}

	/**
	 * @return The duck's height
	 */
	public double height() {
		return 50 * size;
	}


	/**
	 * @return Current X position
	 */
	public double getX() {
		return pos.getX();
	}

	/**
	 * @return Current Y position
	 */
	public double getY() {
		return pos.getY();
	}
	
	/**
	 * @return Current X movement
	 */
	public double getDeltaX() {
		// TODO
		return 0;
	}

	/**
	 * @return Current Y movement
	 */
	public double getDeltaY() {
		// TODO
		return 0;
	}

	@Override
	public String viewRef() {
		return "ellipse:#ee9900";
	}
	@Override
	public void draw(Canvas canvas, Turtle turtle) {
	 Point pos = position();
		// go to our current coordinates
		canvas.color(Colors.BROWN);
		canvas.drawEllipse(pos, width(), height());
		Point head = pos.add(Direction.absolute(30), width()/2);
		canvas.drawEllipse(head, .6*width(), .6*height());
		//turtle.jumpTo(pos.getX(), pos.getY());
		// draw an ellipse, filled with bodyColor
		//turtle.pen(Colors.BROWN).strokeOnly();
		//for(int i = 0; i < 36; i++) {
		//	turtle.draw(5).turn(10);
	///	}
		//turtle.done();
/*		turtle.shape().ellipse().width(getWidth()).height(getHeight()).fillPaint(bodyColor).fill();
		// jump to a position 50*size away at 30° angle, for drawing the head
		turtle.turn(30);
		turtle.jump(getWidth()/2);
		// turn back so the head will be straight
		turtle.turn(-30);
		// draw head as ellipse with headColor
		turtle.shape().ellipse().width(getHeight()).height(0.6*getHeight()).fillPaint(headColor).fill();
		// restore painter state
		turtle.restore();
		drawHealth(turtle);
*/
	}

}
