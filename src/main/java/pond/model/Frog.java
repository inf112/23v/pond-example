package pond.model;

import turtleduck.canvas.Canvas;
import turtleduck.colors.Color;
import turtleduck.colors.Colors;
import turtleduck.geometry.Direction;
import turtleduck.geometry.Point;
import turtleduck.shapes.Ellipse.EllipseBuilder;
import turtleduck.turtle.Turtle;

/**
 * A duck class – recipe for making duck objects.
 * 
 * Describes the common aspects of and behaviour of duck objects (in methods)
 * and the individual differences between ducks (in field variables).
 */
public class Frog extends AbstractPondDweller {
	// Dette er datastrukturen (sett av feltvariabler) som lagrer tilstanden til
	// andeobjektene:
	// "private" betyr at vi ønsker at dette skal være skjult, og at feltvariablene
	// ikke kan
	// endres eller leses av objekter (av andre klasser)
	public boolean jumping = false;
	public boolean croaking = false;
	public int step = 0;
	public double linearStep;
	public double jumpStageA, jumpStageB;
	public double croakStage;
	public double stepVariant;
	public int blinking;
	public int tongue;

	public Frog() {
		this(50, 20);
	}

	public Color getPrimaryColor() {
		return Colors.GREEN;
	}

	public double size() {
		return size;
	}

	public Frog(double x, double y) {
		this.pos = new MyPoint(x, y);
		this.size = 1 + Math.max(0, Math.pow(Pond.random.nextGaussian() / 2, 2));
		this.size = 1;
	}

	public Frog(MyPoint pos, double size) {
		this.pos = pos;
		this.size = size;
	}

	public Frog(double x, double y, double size) {
		this(x, y);
		this.size = size;
	}

	// Dette er definisjonen av metodene som utgjør oppførselen til andeobjektene
	// – brukes til å observere og manipulere endene

	// "public" betyr at objekter av andre klasser har lov å kalle disse metodene

	public void step(Pond world) {
		// i stedet for vanlig Java-valg av hvilken kode som blir kjørt,
		// så vil vi kjøre koden som er i PondDweller (superklassen)
		// (hvis vi sier bare step() eller this.step() – så vil Java se på klassen
		// til objektet, og finne metoden der, eller i en superklasse)
		super.step();
		int slow = 0;
		if (jumping) {
			int speed = (int) (1 + 3 * stepVariant);
//			health = health * 0.999;
			double stepLength = (3 * speed * (1 + size));
			linearStep = Math.min(1.0, step % stepLength / (stepLength - 1));
			jumpStageB = Math.pow(Math.sin(3 * Math.PI / 2 - 2 * Math.PI * linearStep), 1);
			// jumpStageA *= jumpStageA;
			jumpStageA = linearStep > .25 && linearStep < .75 ? (1 + jumpStageB) / 2
					: (1 + (Math.sin(Math.PI * (jumpStageB) / 2))) / 2;
			jumpStageA = (1 + (Math.sin((1 + 0.7 * Math.abs(linearStep - 0.5)) * Math.PI * (jumpStageB) / 2))) / 2;
			jumpStageA -= 0.073;
			if (linearStep < 1.0) {
				pos.move((width() / (1 + 1 * stepVariant)) * jumpStageA, 0);
			} else {
				jumping = false;
			}

			step++;
		} else if (croaking) {
			int speed = (int) (slow + 10 + 3 * stepVariant);
			linearStep = Math.min(1.0, step % (10 * speed) / (9.0 * speed));
			croakStage = Math.sin(Math.sin(Math.sin(Math.PI * linearStep))); // +
																				// Math.abs(Math.sin(slow*Math.PI*linearStep))*.05;
			if (linearStep >= 1.0)
				croaking = false;
			step++;
		} else if (tongue > 0) {
			tongue -= 15;
		} else if (Pond.random.nextInt((int) (1 + 10 * size)) == 0) {
			stepVariant = Pond.random.nextDouble();
			if (Pond.random.nextInt(5) < size)
				croaking = true;
			else
				jumping = true;
			step = 0;
		} else if (Pond.random.nextInt((int) (1 + 100 * size)) == 0) {
			tongue = 180;
		}

		if (Pond.random.nextInt((int) (1 + 50 * size)) == 0) {
			blinking = 3;
		} else if (blinking > 0) {
			blinking--;
		}
		if (pos.getX() > world.width() / 2)
			pos.move(-world.width(), 0);
		if (pos.getX() < -world.width() / 2)
			pos.move(world.width(), 0);
		if (pos.getY() > world.height() / 2)
			pos.move(0, -world.height());
		if (pos.getY() < -world.height() / 2)
			pos.move(0, world.height());
		positionUpdated();

	}

	/** @return The duck's width */
	public double width() {
		return 1.5*70 * size;
	}

	/** @return The duck's height */
	public double height() {
		return 1.5*30 * size;
	}

	/** @return Current X position */
	public double getX() {
		return pos.getX();
	}

	/** @return Current Y position */
	public double getY() {
		return pos.getY();
	}

	/** @return Current X movement */
	public double getDeltaX() {
		// TODO
		return 0;
	}

	/** @return Current Y movement */
	public double getDeltaY() {
		// TODO
		return 0;
	}

	@Override
	public String viewRef() {
		return "ellipse:#008800";
	}


	private Color bodyColor;
	private double size;
	private Color headColor;

	/**
	 * @param turtle
	 */
	@Override
	public void draw(Canvas canvas, Turtle turtle) {
		turtle.jumpTo(position().add(0,3* height() * jumpStageA * (0.5 + stepVariant)));
		turtle.turnTo(-10);

		bodyColor = getPrimaryColor();
		headColor = bodyColor.darker();
		size = size();
		drawLeg(turtle.spawn().turn(5).color(bodyColor.darker()), jumpStageA);

		turtle.spawn(t -> {
			t.turn(25 - 10 * jumpStageA);
			t.jump(.75 * width() / (2.2 - 0.4 * jumpStageA));
			t.turn(-25 + 10 * jumpStageA);
			Point point = t.point();
			Direction direction = t.direction();
			canvas.ellipse().at(point).width(height() * croakStage * 1.).height(height() * croakStage * 1.)
					.fill(headColor.mix(Colors.YELLOW, croakStage / 2)).fill();
		});

		// draw an ellipse, filled with bodyColor
		turtle.spawn(t -> {
			Point point = t.point();
//			canvas.ellipse().at(point).width(getWidth()).height(getHeight()).fill(bodyColor).fill();
		});
		turtle.spawn(t -> {
			t.turn(-170 + 40 * jumpStageA);
			t.jump(width() / 2).color(bodyColor).fillOnly();
			t.turn(-120).draw(width() / 7).turn(-45).draw(width() / 3).turn(-10).draw(width() / 5).turn(-10)
					.draw(width() / 5);
			Point point = t.point();
			Direction direction = t.direction();
			drawArm(t.spawn().color(bodyColor.darker().darker()).strokeOnly().turn(0), jumpStageA);
			t.turn(-60).draw(width() / 10).turn(-70).draw(width() / 7).turn(-20).draw(width() / 3).turn(-30)
					.draw(width() / 3);
			t.turn(-30).draw(width() / 7);
			t.turn(45 * jumpStageA * (0.5 + stepVariant));
//		});
		// jump to a position 50*size away at 30° angle, for drawing the head
//		turtle.spawn(t -> {
//			t.turn(25 - 15 * jumpStageA);
//			t.jump(getWidth() / (2 - 0.4 * jumpStageA));
			// turn back so the head will be straight
			t.jumpTo(point).turnTo(direction);
			canvas.ellipse().at(point).width(height()).height(0.6 * height()).fill(headColor).fill();
			if (tongue > 0) {
				double l = Math.sin(Math.toRadians(tongue));
				t.spawn().color(Colors.PINK).stroke(Colors.RED, 3).jump(.5 * height()).draw( 5 * l * height()).done();
			}
			t.turn(120).jump(.31 * height());
			point = t.point();
			EllipseBuilder e = canvas.ellipse().at(point).width(.2 * height()).height(0.2 * height()).fill(Colors.YELLOW.alpha(0.9));
			canvas.ellipse().at(point).width(.3 * height()).height(0.3 * height()).fill(bodyColor).fill();
			e.fill();
			canvas.ellipse().at(point.add(.2, 0)).width(.02 * height()).height(0.1 * height()).fill(Colors.BLACK)
					.fill();
			if (blinking > 0)
				canvas.ellipse().at(point).width(.3 * height()).height(0.3 * height()).fill(bodyColor).fill();

			// draw head as ellipse with headColor
			// turtle.shape().ellipse().width(getHeight()).height(0.6 *
			// getHeight()).computedFill(headColor).fill();
			// restore painter state
		});
		drawLeg(turtle.spawn().color(bodyColor.darker().darker()), jumpStageA);

		// draw an ellipse, filled with bodyColor

	}

	public void drawArm(Turtle turtle, double jumpStage) {
		Color c = bodyColor.mix(Colors.BROWN, .15).darker();
		Color c2 = bodyColor.mix(Colors.FORESTGREEN, .25);
		c = c.mix(c2, .2);

		turtle.turn(-150);
		turtle.turn(15).jump(width() / 10).turn(-15);
		turtle.penChange().stroke(c, 3 * size).done();
		turtle.draw(width() / 3);

		c = c.mix(c2, .2);
		turtle.turn(125 - 140 * jumpStage);
		turtle.penChange().stroke(c, 3 * size).done();
		turtle.draw(width() / 3);

		c = c.mix(c2, .2);
		turtle.penChange().stroke(c, 2 * size).done();
		turtle.spawn().turn(-15 + 35 * jumpStage).draw(width() / 8);
		turtle.spawn().turn(-105 + 135 * jumpStage).draw(width() / 12);
	}

	public void drawLeg(Turtle turtle, double jumpStage) {
		// Math.abs((step % 100) - 50) / 50.0;
		// legs

		Color c = bodyColor.mix(Colors.BROWN, .15).darker();
		Color c2 = bodyColor.mix(Colors.FORESTGREEN, .25);
		c = c.mix(c2, .2);

		turtle.turn(-170 + 40 * jumpStage);
		turtle.penChange().stroke(c, 8 * size).done();
		turtle.jump(width() / 2.5);
		turtle.turn(-(170 + 40 * jumpStage));

		c = c.mix(c2, .2);
		turtle.turn(-25 - (160 * jumpStage));
		turtle.penChange().stroke(c, 6 * size).done();
		turtle.draw(width() / 3);

		c = c.mix(c2, .2);
		turtle.turn(-160 + (165 * jumpStage));
		turtle.penChange().stroke(c, 5 * size).done();
		turtle.draw(width() / 3);

		c = c.mix(c2, .2);
		turtle.turn(165 - (160 * jumpStage));
		turtle.penChange().stroke(c, 4 * size).done();

		turtle.draw(width() / 6);

		c = c.mix(c2, .2);
		turtle.turn(0 - 45 * jumpStage);
		for (int i = -1; i < 2; i++) {
			int k = i;
			Color col = c.mix(c2, .2);
			turtle.spawn(t -> {
				t.turn(k * 3 * jumpStageB);
				t.penChange().stroke(col, 1.5 * size).done();

				t.draw(width() / 6);
				t.turn(-k * 1 * jumpStageB + 115 * jumpStage * (1 - jumpStage));
				t.penChange().stroke(col, 1.5 * size).done();

				t.draw(width() / 6);
			});
		}

	}

}
