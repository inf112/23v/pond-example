package pond.model;

import pond.controller.ModelUpdateListener;
import pond.view.Viewable;
import turtleduck.canvas.Canvas;
import turtleduck.geometry.Point;
import turtleduck.turtle.Turtle;

public interface PondObject extends Viewable {
	/**
	 * Do one time step of actions for the object
	 */
	void step(Pond world);
	
	String viewRef();
	
	Point position();

	double width();
	
	double height();
	
	double getX();
	double getY();

	void addListener(ModelUpdateListener listener);

	void draw(Canvas canvas, Turtle turtle);
}
